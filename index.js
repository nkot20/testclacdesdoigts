const express = require('express');
const dbConnect = require('./config/dbConnect');
const app = express();
const dotenv = require('dotenv').config();
const chickenRoutes = require('./routes/chickenRoutes');
const farmyardRoutes = require('./routes/farmyardRoutes')
const bodyParser = require('body-parser');
const { notFound, errorHandler } = require('./middlewares/errorHandler');
const fs = require('fs');

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger/swagger.json');
const customCss = fs.readFileSync((process.cwd()+"/swagger/swagger.css"), 'utf8');

const PORT = process.env.PORT || 4000

dbConnect();
//console.log(swaggerDocument);
app.use(bodyParser.json());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, {customCss}));

app.use(bodyParser.urlencoded({ extended: false }))
app.use(process.env.BASE_URL+'/chicken', chickenRoutes);
app.use(process.env.BASE_URL+'/farmyard', farmyardRoutes);

app.use(notFound);
app.use(errorHandler);

app.listen(PORT, ()  => {
    console.log(`Server is running at PORT ${PORT}`);
});

module.exports = {
    app: app
}