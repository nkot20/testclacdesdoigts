const mongoose = require('mongoose'); // Erase if already required

// Declare the Schema of the Mongo model
var FarmyardSchema = new mongoose.Schema({
    location:{
        type:String,
        required:true,
    },
    owner:{
        type:String,
        required:true,
    },
    area:{
        type:String,
        required:true,
    },
    
}, {
    timestamps: true,
});


//Export the model
module.exports = mongoose.model('Farmyard', FarmyardSchema);