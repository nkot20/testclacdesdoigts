const mongoose = require('mongoose'); // Erase if already required

// Declare the Schema of the Mongo model
var ChickenSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
    },
    birthday:{
        type:Date,
    },
    weight:{
        type:Number,
        required:true,
    },
    steps:{
        type:Number,
        default: 0
    },
    isRunning:{
        type:Boolean,
        default: false
    },
    _idFarmYard: {
        type: String,
        required: true
    }
    
}, {
    timestamps: true,
});


//Export the model
module.exports = mongoose.model('Chicken', ChickenSchema);