const Chicken = require('../models/chicken')
const Farmyard = require('../models/farmyard')
const asyncHandler = require("express-async-handler");

//add new chicken
const createChiken = asyncHandler(async (req, res) => {
    
    try {
        if(!req.body._idFarmYard) {
            throw new Error("You need to specify farmyard with a ID, read documentation please");
        } 
        //console.log(req.body._idFarmYard);
        const _idFarmyard = await Farmyard.findById(req.body._idFarmYard);
        
        if(!_idFarmyard) {
            throw new Error("Farmyard does not exist");
        }
        const chicken = await Chicken.create(req.body)
        res.json(chicken)
    } catch (error) {
        throw new Error(error)
    }
    
})

//update chiken
const updateChicken = asyncHandler(async (req, res) => {
    
    const {id} = req.params;
    try {
       const updateChicken = await Chicken.findByIdAndUpdate(
        id, 
        {
            name: req?.body?.name,
            birthday: req?.body?.birthday,
            weight: req?.body?.weight,
            steps: req?.body?.steps,
            isRunning: req?.body?.isRunning
        },
        {
            new: true
        }
       );

       res.json(updateChicken)
    } catch (error) {
       throw new Error(error) 
    }
})

//Get all chickens
const getAllChickens = asyncHandler(async (req, res) => {
    try {
        const getChicken =  await Chicken.find();
        res.json(getChicken)
    } catch (error) {
        throw new Error(error)
    }
});

//get single chicken
const getChicken = asyncHandler(async (req, res) => {
    const {id} = req.params;
    try {
        const findChicken = await Chicken.findById(id);
        
        res.json(findChicken)
    } catch (error) {
      throw new Error(error)  
    }
})

//delete single chicken
const deleteChicken = asyncHandler(async (req, res) => {
    const {id} = req.params;
    
    try {
        const deleteChicken = await Chicken.findByIdAndDelete(id);
        
        res.json(deleteChicken)
    } catch (error) {
      throw new Error(error)  
    }
})


// method to increase the steps variable by 1
const run = asyncHandler(async (req, res) => {
    const {_id} = req.body;
    try {
       const findChicken = await Chicken.findById(_id);
       const updateChicken = await Chicken.findByIdAndUpdate(
        _id, 
        {
            steps: findChicken.steps + 1,
        },
        {
            new: true
        }
       );
       
       res.json(updateChicken)
    } catch (error) {
       throw new Error(error) 
    }
})

module.exports = {
    createChiken, 
    updateChicken, 
    getAllChickens,
    getChicken,
    deleteChicken,
    run
}
