const Chicken = require('../models/chicken')
const Farmyard = require('../models/farmyard')
const asyncHandler = require("express-async-handler");

const createFarmyard = asyncHandler(async (req, res) => {
    
    try {
        const farmyard = await Farmyard.create(req.body)
        res.json(farmyard)
    } catch (error) {
        throw new Error(error)
    }
    
})

//Get all farmyards
const getAllFarmyards = asyncHandler(async (req, res) => {
    try {
        const getFarmyard =  await Farmyard.find();
        res.json(getFarmyard)
    } catch (error) {
        throw new Error(error)
    }
});

module.exports = {
    createFarmyard,
    getAllFarmyards
}