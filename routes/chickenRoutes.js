const express =  require('express');
const { createChiken, getChicken, deleteChicken, getAllChickens, updateChicken, run } = require('../controller/chickenCtrl');
const router = express.Router()

router.post('/add', createChiken);
router.put('/update/:id', updateChicken);
router.patch('/run', run)
router.get('/all', getAllChickens);
router.get('/:id', getChicken);
router.delete("/delete/:id", deleteChicken);

module.exports = router