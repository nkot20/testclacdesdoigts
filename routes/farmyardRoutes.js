const express =  require('express');
const { createFarmyard, getAllFarmyards } = require('../controller/farmyardCtrl');
const router = express.Router()

router.post('/add', createFarmyard);
router.get('/all', getAllFarmyards);

module.exports = router