var app = require('../index.js').app,
//assert = require('assert'), 
chai = require('chai'),
//should = require('should'); 
request = require('supertest'),
should = require('should');
var id = ""

//test add chicken
describe('add Chicken', function() { 
  it('responds with json', function(done) { 
    request(app) 
    .post(process.env.BASE_URL+'/chicken/add') 
    .send(
        {
            "name": "test3",
            "birthday": "2023-02-31T23:00:00.000+00:00",
            "weight": 15,
            "steps": 0,
            "isRunning": false,
            "_idFarmYard": "648ed473ef4c952434160c52"
        }
    ) 
    .set('Accepter', 'application/json') 
    .expect('Content-Type', /json/) 
    .expect(200)
    .end(function(err, res ) { 
        if (err) return done(err); 
        console.log(res._body._id)
        id = res._body._id
        done(); 
      }); 
  }); 
});

//test update chicken
describe('add chicken', function() { 
  it('responds with json', function(done) { 
    request(app) 
    .put(process.env.BASE_URL+'/chicken/update/'+id) 
    .send(
        {
            "name": "test3",
            "birthday": "2023-02-31T23:00:00.000+00:00",
            "weight": 17,
            "steps": 0,
            "isRunning": true,
            "_idFarmYard": "648e2cd4576664874a9608a3"
        }
    ) 
    .set('Accepter', 'application/json') 
    .expect('Content-Type', /json/) 
    .expect(200)
    .end(function(err, res ) { 
        if (err) return done(err); 
        console.log(res._body._id)
        id = res._body._id
        done(); 
      }); 
  }); 
});

//test get all chicken
describe('get all chickens', function() { 
  it('responds with json', function(done) { 
    request(app) 
    .get(process.env.BASE_URL+'/chicken/all') 
    .set('Accepter', 'application/json') 
    .expect('Content-Type', /json/) 
    .expect(200)
    .end(function(err, res ) { 
        if (err) return done(err); 
        done(); 
      }); 
  }); 
});

//test get single chicken
describe('get single chicken', function() { 
  it('responds with json', function(done) { 
    request(app) 
    .get(process.env.BASE_URL+'/chicken/'+id) 
    .set('Accepter', 'application/json') 
    .expect('Content-Type', /json/) 
    .expect(200)
    .end(function(err, res ) { 
        if (err) return done(err); 
        done(); 
      }); 
  }); 
}); 

//test run chicken
describe('run chicken', function() { 
  it('responds with json', function(done) { 
    request(app) 
    .patch(process.env.BASE_URL+'/chicken/run') 
    .send(
      {
        "_id": id
      }
    ) 
    .set('Accepter', 'application/json') 
    .expect('Content-Type', /json/) 
    .expect(200)
    .end(function(err, res ) { 
        if (err) return done(err); 
        done(); 
      }); 
  }); 
});

//test get single chicken
describe('delete a chicken', function() { 
  it('responds with json', function(done) { 
    request(app) 
    .delete(process.env.BASE_URL+'/chicken/delete/'+id) 
    .set('Accepter', 'application/json') 
    .expect('Content-Type', /json/) 
    .expect(200)
    .end(function(err, res ) { 
        if (err) return done(err); 
        done(); 
      }); 
  }); 
}); 



