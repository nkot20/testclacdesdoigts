var app = require('../index.js').app,
//assert = require('assert'), 
chai = require('chai'),
//should = require('should'); 
request = require('supertest'),
should = require('should');

//test add farmyard
describe('add farmyard', function() { 
  it('responds with json', function(done) { 
    request(app) 
    .post(process.env.BASE_URL+'/farmyard/add') 
    .send(
        {
          "location": "farm1",
          "owner": "nkot",
          "area": "20"
      }
    ) 
    .set('Accepter', 'application/json') 
    .expect('Content-Type', /json/) 
    .expect(200)
    .end(function(err, res ) { 
        if (err) return done(err); 
        done(); 
      }); 
  }); 
});

//test get all chicken
describe('get all farmyards', function() { 
  it('responds with json', function(done) { 
    request(app) 
    .get(process.env.BASE_URL+'/farmyard/all') 
    .set('Accepter', 'application/json') 
    .expect('Content-Type', /json/) 
    .expect(200)
    .end(function(err, res ) { 
        if (err) return done(err); 
        done(); 
      }); 
  }); 
});